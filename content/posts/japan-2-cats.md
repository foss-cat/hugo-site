---
title: "Japan 2 Cats"
date: 2022-11-20T01:00:59-07:00
draft: false
tags: ['japan', 'travel', 'cats']
summary: 'Our second day was full of cats, visiting Nippori and walking down the "shotengai", or the main shopping street.'
tocOpen: true
cover:
  image: "/images/japan-nippori-walk.JPG"
  alt: ""
  caption: "Walking through Niporri's shotenga"
  relative: false
---

# Japan is Fond of Their Cats

My wife and I have two cats of our own, so naturally we had to experience the cat love Japan had to give. When you walk down any main street, you can't go many *meters* (making my nod to the metric) without seeing some sort of cat on billboards, store front signs, etc. We found a hosted experience of a 'walk in a cat-loving community' and had to try it. We planned most of our day around meeting En at 1pm in Nippori. 

## Morning - Garden Walk

![image](/images/japan-shinjuzu-garden.JPG#center)

We had our equivalent of a 'continental' breakfast at our hotel, which was leaps ahead of American hosting hospitality standards. Plus, it was largely fats and protiens, which is a much better way to check off consuming your 'most important meal of the day'. We then headed to Shinjuku National Garden - $5 admission, and enjoyed the h\*ck out of the 65 degree weather. We walked and viewed the grounds, the whole garden is probably a mile across. Before we left we found a little bench in the sun for my wife to paint, she brought a pocket watercolor set. Away to Niporri to meet En and cat city.

## Afternoon - Cat City

En is from Japan, learned English in Scottland of all places (she spoke english with a Japanese accent instead of Scottish, which disappointed me just a little), and was such a kind guide. We walked down the shotengai of Nippori, the main shopping street of a city, exploring all the little shops on each side. Each store was themed it seemed, like bamboo goods, paulownia wood boxes, and of course, cat shops, selling things to show your love for cats, not to care for them. We loved it. There were also a number of local art galleries, often hosted inside historic homes in the neighborhood that had been renovated. It was really interesting to walk through these renovated-homes-converted-art-cafes and see traditional alongside contemorary art by the local residents, while people order teas and coffee at the cafe.

![image](/images/japan-nekomachi.JPG#center)

This house called itself the nekomachi, or cat-town of the neighborhood, with a gallery inside of exclusively cat-themed art.

It was probbably the most 'local' thing we will do on our trip, we loved it. We ended our walk in Niporri passing through the neighborhood's 'graveyard', in the word of En, though I think the western idea of graveyard is tainted with Halloween spooky connotations. En told us that it has been full for probably 30 years, with any new bodies desiring to be buried needing to relocate further from Tokyo. Some of the gravesites had massive stones with characters carved into it's face. En told us they chronicled the person's life and their family, not unlike a westerner's tombstone, just more verbose. I should mention that we stopped and purchased ourselves some taiyaki, a fish pastry filled with sweet red bean paste, and my wife was crazy for it. They are pretty good.

![image](/images/japan-nippori-graveyard.JPG#center)

## Evening - Cat Cafe and Ramen

We ended our evening visiting Akihabara. We were hoping to enjoy its renowned anime consumer content, but were disappointed to not find any stickers or shirts, just pins, figurines, charms, manga, flags, posters, chibis, funko pops, and probably every other piece of parafanelia. I guess the Japanese just do it differently. We were pretty hungry so we headed over to the cat cafe. You pay for time and get to use an auto dispensing hot/cold drink machine (coffee, tea, cocoa) and pet any of the 20+ cats in the space. It had a good vibe, and the cats were beyond my comprehension soft. They also were all so aloof, they get so much attention and stimulation they can get anything they want, so they all were very uninterested in you. Felt insulting. We finished the evening by getting some genuine 'pork oil noodle' ramen at a walk-up ramen bar. The taste was pretty awesome, just had to get over the idea of them pouring pork oil on the noodles.

We had a super fun second day.  Felt less wiped at the end of the day, but our feet were aching. I was wearing my minmalist flip-flops because the weather was so nice, but that is probably going to end.


## My Japan Travel Tips (JTT)

I think there will be a number of things I will learn over the course of this trip that could be helpful if you ever travel to Japan, and if not, they are interesting nonetheless.

- Coming from the U.S., managing water and trash is difficult at first. There are many conbinis (convenience stores) that sell water bottles, and inside you can find trash cans. There aren't really any other convenient ways to fill water or toss trash.

- Steering wheels are on the right side, so everything is reversed. Mostly this just means people will pass you on the sidewalk on the left side, and it makes for lots of _you and the person walking toward you both move to the right, then it gets awkward_ moments.