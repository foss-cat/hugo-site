---
title: "How I Made This Site"
date: 2022-08-31T20:38:09-06:00
draft: false
searchHidden: false
tags: ['self host', 'raspberry pi']
summary: 'I talk about how the "cloud" works, and show how one can host a site on the internet'
tocOpen: true
cover:
  image: "/images/ocean-aerial.jpg"
  alt: ""
  caption: "The ocean, like my mind, is really big"
  relative: false
---

I've dreamed of this day but never imagined I'd actually think I've got ideas mature enough to do it. Turns out, kids can have good ideas too once in a while.

# How do websites work?
Back in my senior year of highschool, my buddies and I thought it would be funny to have our own blog / website. I didn't think there was much to it, so I turned to godaddy.com, the only website-buying marketplace around. Little did I know that they only sell domain names, and what I could even do with one. Did I pay $20 for a domain name? Splashthetown.com sat vacant for exactly one year under my command. Two-thirds of a CS degree later and I think I am finally ready. Billions of webpages exist on the internet, so it can't be that difficult, right? I'm going to walk you through the steps I undertook to get this website to you, and how you are able to see the words you are reading on your screen right now.

## Internet, I've Heard of That

I once heard the internet described as "the cloud", which is good to help people understand you know nothing about it. To give a marginally better explanation, imagine your brain, with all its neurons interconnected and whatnot. Lets call each neuron a "node". Each node holds information, and when it recieves a message it decides what to do with that information, modify it, store it, pass it on, sell it to the highest bidder for ad revenue, the possibilities are endless. In this way, the brain is much like the internet. These "nodes", or nuerons, are actually computers that make up the internet, a big web of interconnected, communicating devices. Our goal is to add a node to the network, and tell it to send specific information to anyone who calls on it.
<!--
How did I get Here
---
Here is the process when you type in or click on a link in your web browser:
 - Your machine sends a **request** with the link info to the "node" or machine giving it internet, typically a router.
 - The router _routes_ your request to an internet "phone book" of sorts to see what machine on the interwebs your url cooresponds to, much like an address on a package you send in the mail.
 - With the machines (ip) **address** known, your package is sent to that machine
 - The machine has some code running on it that opens your package, gives thanks, and as quickly as it can manage, sends a package back to the sender with some code.
 - That package goes back down the pipes the way it came, your browser opens the package, and you see the package contents, `html`, Mark Zuckerburg pics, cat videos, etc.
-->
# How to create your own _Node_

If you want to put your stake on the great world wide web, then you need a few things:

 - The _Address_ - a domain name, purchasing one lets you tell internet phone books where your computer is located in the web.
 - The _Node_ - a computer that can run some internet code on it. Most people want this to be some computer AWS or other server farm controls so that it stays online 24/7 and can scale easily (your code can be copied to other machines if you get tons of visitors). But it can get boring if you don't have to keep fixing things on your server.
 - The _Code_ - Some call it a web server, I adhere to this practice.

I will be walking you through the steps I took to get you here on this web page. There are hundreds of ways to get something on the internet, and my way is certainly one of them. For reference, I am running Arch Linux btw on my main computer.

Since I don't want to pay $5 a month for ease-of-use and stability and scalability, I will be using a rasperry pi zero w2 plugged into a charging brick behind my book shelf to be the node.

![image](/images/rpi-bookshelf.jpg#center)
My Rasperry Pi Zero w2 - How it is currently serving up the web

## Get a Domain

Any self-respecting, domain-selling company online will happily take your money. I've used [Name](https://name.com) and [GoDaddy](https://godaddy.com) (but not really), but any google result will do. Purchase one, that is all for now. This probably didn't need a whole section...

## Write some Internet Code

Actually the goal for me with this site is to not have to write any code at all.

If you don't have a raspberry pi and instead coughed over $$$ to Jeff Bezos, then get the IP address of your machine and skip to step 4.

1. I used [rpi imager](https://github.com/raspberrypi/rpi-imager) to flash Raspberry Pi OS Lite to an SD card.
    - You can use any flashing tool, but rpi imager is convenient when working with Pi's, and it lets you configure Wifi so the Pi connects automatically on boot.
2. Put the SD card into the Pi and turn it on.

3. Verify it is connected to the internet:
    - I use [nmap](https://nmap.org/) to scan my local network

    ``` s
        nmap -sn 192.168.<insert third octet from device ip here, like 27>.0/24
    ```
    This gives me output like
    ``` s {hl_lines=[4]}
        Host is up (0.018s latency).
        Nmap scan report for 192.168.27.169
        Host is up (0.077s latency).
        Nmap scan report for raspberrypi.lan (192.168.27.161)
    ```

4. Use ssh to connect to the device `ssh user@<ip address of server>`. Do _not_ forget to include the user name in the ssh command.

5. For this webserver, I'm using [Hugo](https://gohugo.io/), a static site generator written in [Go](https://go.dev/) to create the pages, and [nginx](https://www.nginx.com/resources/wiki/start/) to serve the files. Follow the instructions [here](https://gohugo.io/getting-started/quick-start/#step-1-install-hugo) to install it to your server / _node_ and set up a simple site with Hugo. Optionally, you can build your site locally and then share it to your server with git, but that is out of scope for this post.
    - Once your site is built, run `hugo` in the base directory of your hugo site. This will generate all the static files for your website. It gave me this output:
        ``` s
            user@pi:~/foss-cat $ hugo
            Start building sites … 
            hugo v0.102.2-f104f7bc02f7d23d41f17c7353df1217cf25b2dc linux/arm BuildDate=2022-08-31T10:42:06Z VendorInfo=gohugoio

                            | EN  
            -------------------+-----
            Pages            | 10  
            Paginator pages  |  0  
            Non-page files   |  0  
            Static files     |  0  
            Processed images |  0  
            Aliases          |  2  
            Sitemaps         |  1  
            Cleaned          |  0  

            Total in 756 ms
        ```

6. With your hugo pages built, install nginx and configure it to serve those pages. I found [Gideon Wolfe's guide](https://gideonwolfe.com/posts/sysadmin/hugonginx/) to be super useful. On my raspberry pi I ran
    ``` sh
        sudo apt-get install nginx
        sudo /etc/init.d/nginx start
    ```

    Then `cd` into the `sites-available` config of nginx and create a new file, call it something creative like `<your-domain>`. In it, put this configuration, changing lines 5 and 7 to their proper names:
    ``` sh {linenos=true,hl_lines=[5,7]}
    server {
       listen 80;
       listen [::]:80;

       server_name mysite.com www.mysite.com; # Domain name you purchased

       root /home/<my-username>/<my-hugo-site>/public/; # Absolute path to your hugo site
       index index.html; # Hugo generates HTML

       location / {
               try_files $uri $uri/ =404;
       }
    }
    ```
    Now, create a sym-link of the new file into the directory `sites-enabled`. Don't use relative paths, I got weird nginx errors :no_mouth: :
    ``` sh
    sudo ln -s /etc/nginx/sites-available/<config_file> /etc/nginx/sites-enabled/<config_file>
    ```
    With that complete, run `sudo systemctl restart nginx.service` and you should be able to visit the ip address of your machine and see your hugo site!

7. Add domain name records through your domain name provider. You probably just need two A records, one with and without "www". It should look something like this:
    | Type | Host           | Answer       |
    |------|----------------|--------------|
    | A    | www.<mydomain> |  ip address  |
    | A    | <mydomain>     |  ip address  |

    - If you are self-hosting like I am, you need to port-forward your device to make it visible on the world-wide-web. The steps to do this vary depending on your router. I fortunately and unfortunately have Google Fiber. So I can download Warzone in an hour but my resistance to an all-knowing data-collecting monolith feels futile. To port forward, you want to lock your raspberry pi's IP assigned by the router. This is done through DHCP. Then, open up your router's external port to the cooresponding internal raspberry pi port. For http you want your pi's port `80`. If you use SSL (which you should, its easy to setup), then use port `443`.

    :exclamation: DISCLAIMER :exclamation:

    Port-Forwarding was something I have always wanted to do but my loving father never let me do it as a kid because he said bad stuff could happen, i.e stranger danger. I don't think self-hosting is super safe or reasonable for most use-cases today, but I'm willing to do it because its awesome and I've always wanted to (see above). You should definitely lock down your local device with a good password and enforce SSH with trusted keys only. To do that, run this command on your regular machine (laptop) to your raspberry pi.
    ``` sh
    ssh-copy-id <user>@<ip of pi>
    ```
    SSH into the pi and go to the file `/etc/ssh/sshd_config` and modify line 58 to read:
    ``` sh {linenos=true,hl_lines=2,linenostart=57}
        # To disable tunneled clear text passwords, change to no here!
        PasswordAuthentication no
        #PermitEmptyPasswords no
    ``` 
    Exit and SSH back in to see if it works.
    Now reboot your machine. Proceed with cautious optimism.
    
    You've been warned.

8. Lastly, I used [certbot](https://certbot.eff.org/) to add SSL to my site, because its easy and it makes your site's sketchiness go from back-street alley to across-the-street mailman. On my Raspberry Pi I ran:
    ``` sh
        sudo apt install snapd  # I didn't want to install snap on such a resource-limited device, but it was the recommended way, and I didn't want to trailblaze any longer, especially with ssl configuration, a subject I know little about
        sudo snap install core
        sudo snap install --classic certbot
        sudo ln -s /snap/bin/certbot /usr/bin/certbot  # Adds cert-bot to your PATH if /snap/bin isn't configured, you may need to restart / log-off log-in for the last line
        sudo certbot --nginx
    ```

# Spread your Propaganda


If you followed this guide and got a working site, [send me an email](mailto:tom@fosscat.com). I would love to check it out. This is my very first blog post and my very first blog site. I spent a whole lot of time setting everything up just how I like it, and I'm pretty happy with how it all turned out. I think the actual setting up of the website probably only took a few hours, but writing this and getting all the styles and aesthetics to look the way I want took 10+ hours.

I also decided near the end that I would open-source my hugo pages, so if you find any mistakes, want to copy the styles, or just criticize my ideas, feel free to follow the `Suggest Changes` link above or click [here](https://gitlab.com/foss-cat/hugo-site/) to let me know how you feel.

'Til next time


