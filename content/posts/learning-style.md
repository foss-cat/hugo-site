---
title: "Learning Style"
date: 2022-12-29T15:59:11-07:00
draft: false
tags: ["rant", "learning style"]
summary: "I rant about the myth of learning styles"
tocOpen: true
cover:
  #image: "/images/img.jpg"
  # can also paste direct link from external site
  # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
  alt: ""
  caption: ""
  relative: false
---

# What's Your Learning Style?

Hot take warning.

I think the notion that a person has a preferred learning style is ridiculous. I think it is a learned preference and a widely accepted myth, people learn in all sorts of different ways depending on the subject matter, environment, and age.

## Why do I care?

Because, I think its annoying when people bring up their learning preference to say that they are a visual learner. No one ever brings it up to say they have a tactile or auditory learning style. Why? Kinda like how (in my experience) whenever someone feels the need to unsolicitedly claim they have native american ancestry it is always Cherokee. Nothing against the Cherokee nation, its just funny to me that in my experience no one has broken those molds.

The real bone I have to pick with visual learners is I feel it is used as a crutch to make some claim that they are unable to participate well in school or complete some task because it isn't catered toward their learning style. If anything, today would be the greatest time in the history of the world to be a visual learner. You can give ai somne words and it creates photo-realistic images. Youtube has more content to watch than you could ever get through in your lifetime, 1000's fold. And that's one platform. But visual learners, in my mind, have a close connotation with lazy zoomer tik-tok scrollers who can 'only learn' DIY hacks presented to them in a rapid fire, meme-spliced format. No. Thats learned through your terrible habits. Put your phone down and try to read a good book. Humans have learned through auditory and written means for thousands of years. Now you are gonna tell me there isn't a single auditory or tactile learner that brings up this inconvenience when Mr. Bones turns on Bill Nye the Science Guy?

To be realistic, this hardly ever happens, but it is irksome to me when it does.

Turns out [Veritasium](https://www.youtube.com/watch?v=rhgwIhB58PA) made a video about this exact thing. He cites reviewing articles that state "There is no credible evidence that learning styles exist", so there we go. Just an intellect's imaginary invention.
