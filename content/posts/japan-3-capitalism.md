---
title: "Japan 3 Capitalism - WIP"
date: 2022-11-20T04:33:54-07:00
draft: false
tags: ['japan', 'travel']
summary: 'For our third day in Tokyo, we shopped til we dropped'
tocOpen: true
cover:
  image: "/images/japan-shopping.JPG"
  # can also paste direct link from external site
  # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
  alt: ""
  caption: "In the heart of Shinjuku, the number of shops is overwhelming. Well, maybe for a novice **smirk*"
  relative: false
---

# Hold on

I have this post half written, we are going to be on a long train ride today so I'll finish this one up then.