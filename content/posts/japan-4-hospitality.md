---
title: "Japan 4 Hospitality"
date: 2022-11-21T05:19:38-07:00
draft: false
tags: ['japan', 'travel', 'culture', 'politics']
summary: 'We packed up from Shinjuku and headed to a small town near the base of Mount Fuji and experienced the generosity and kindness of an old man there.'
tocOpen: true
cover:
  image: "/images/japan-fuji.JPG"
  # can also paste direct link from external site
  # ex. https://i.ibb.co/K0HVPBd/paper-mod-profilemode.png
  alt: ""
  caption: "Mount Fuji view from Lake Kawaguchi"
  relative: false
---

# Japan's Hospitality

Waking up this morning we weren't in a rush, but we felt like we had no idea how to get to our next stay in Oshino, near Mt. Fuji. We packed our bags, which got exceptionally more full in just three days, and walked over to the Shinjuku station. We found some helpful guides to talk us through which tickets we needed and we headed out on a two hour bus ride to Oshino. We drove through some beautiful scenery, greens yellows and reds stretched on before us as our little bus meandered through the twisting highway roads. Near the end of the trip, we rounded a bend that opened up to an impressive vista of Mt. Fuji, peaking through the clouds above us. I was shocked just how high up the peak rose above the surrounding landscape. Once we arrived, our AirBnB host was waiting to give us a lift to his house. He must be the sweetest old Japanese man in Oshino, he treated us so kindly, telling us stories, asking us our plans and giving us advice on where to go. He even drove us to the station we needed to go to tomorrow and helped us get the right tickets in advance. 

## Visiting a Shrine

My wife asked if he knew where any good shrines were in the area, anticipating some directions, and he just said something along the lines of "ah, we go". He took us straight there. He taught us the customs and etiquette of visiting the shrines:
 - When entering through the gate (Torii, the big red curved beam spanning across red columns) you bow twice
 - Also when entering you walk on one side of the path, not the middle. He didn't know the word, but my guess is its reserved for either religious or royal persons.
 - At this shrine there was a fountain with multiple spigots, he told us to wash our hands, but motioned not to rinse your mouth and spit out the water. I didn't have a problem supressing my urge to rinse out my mouth at the sight of running water, so I considered myself lucky.
 - At the back of the grounds is the shrine offering. You toss a 5-50 yen coin in it and bow twice, then clap twice, then bow one more time, during which you make a wish and "remember it in your heart".
 - As you leave, you turn back to shrine and bow once more.

![image](/images/japan-spirit-tree.JPG#center)
Our host told us that its believed that the god lives on top. Its probably 40+ feet around at the base.

Not sure how much got lost in translation, but it seemed we did a good job of following his instructions. He then dropped us off by Lake Kawaguchi, which had a stunning view of Mount Fuji with the sun conveniently setting. He left us to sight see some more as the valley was swallowed in Mt. Fuji's shadow. Wow I'm impressed by my literary devices tonight.

## Walking the Neighborhood

It got dark really quickly, and much cooler than Tokyo got in the evenings. We stopped a local walmart equivalent and purchased some gloves and ramen cups to cook once we got home. We wanted to go out for food, but Mondays are like western Sundays, and every shop we tried was closed. 

![image](/images/japan-fuji-town.JPG#center)

There was one open but it has questionable reviews and pictures on google, so we decided better stick with the MSG than food poisoning. A note I've noticed about portions here in Japan: everything is packaged in really sensibly sized portions. Its almost as if the governing body of Japan and food distributors are colluding together to get us to not over eat. It feels really intrusive.

## Greater Hospitality

Our host was greatly amused at our noodle cup dinner. He heated up some water for us and pulled out wrapped frozen pork he had prepared and told us to heat it up to put in our ramen. It definitely leveled up the quality. 

![image](/images/japan-noodle-cup.JPG#center)

His house is off-grid with solar panels, with lots of custom wood carpentry, he told us its a hobby of his. He also runs a small cafe out of the house that is all organic, he serves food he grows in his little farm. Its a pretty incredible passion project of his, all in an effort he says to reduce his CO2 emissions, as global warming has increased the size and damage of typhoons to Japan. His eletric car also has a big sticker on it about being in some sort of EV club that I suspect views him as the president. As he showed us the house, he mentioned that we could use his 'onsen' (hot spring / public bath). He shows us into a sauna looking room with a large wooden bath. We happily accepted the offer. We had a wonderfully warm evening after some trekking out in the cold.