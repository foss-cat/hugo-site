---
title: "Japan 1 Arrival"
date: 2022-11-18T14:31:04-07:00
draft: false
tags: ['travel', 'japan']
searchHidden: false
summary: 'We arrived to Tokyo, experienced some of the city as we found our way to our hotel for the night.'
tocOpen: true
cover:
  image: "/images/japan-arrival.JPG"
  alt: ""
  caption: "The land of the rising sun greeted us with a setting one."
  relative: false
---

# We Made It

My wife and I finally get to experience first hand all of the things we watched in anime.

## Flight Experience

We had a short flight to Seattle early in the morning, an hour wait, then hopped on the massive 10-people-per-row, serves-you-two-meals passenger plane. I've never flown internationally to this extent, so this was all very new experiece for me. The food was surprisingly delicious, my butt was unsurprisingly sore after a 9.5 hour flight. Things passed honestly quicker than I anticipated. We arrived in Japan at 2PM the next day. Pretty trippy, but sleeping on the plane kinda felt like we didn't just lose a day to the time-zone lords. We were both pretty frazzled after getting off the plane, but there were a ton of Japanese people pointing and holding signs of where to go to get processed through the system. When I was getting my fringerprints scanned, I didn't know it was also taking a picture. My nose was itchy so I did what anyone would do with an itchy nose and occupied hands, I scrunched my face rapidly and desperately. When my fingers were done it showed me my lemon-head motion-blurred image of a fugitive. Hopefully there is no system that flags me. Inside the airport, we purchased a SIM card for one of our phones to have data, and luckily found super friendly english-speaking worker that told us how to get to our hotel. We took a short one hour bus ride from Haneda to Shinjuku where we will be staying for the next three days.

## The City

Immediately after getting off the bus, we found ourselves in the heart of a cement and steel labrynth. I've been to New York, but otherwise the largest city I've been in is Phoenix. Shinjuku (which to my understanding is inside the "bigger" Tokyo, but I think there is a Tokyo, Tokyo, kinda like NY, NY..?) is huge, with every development towering over the people below. Carrying luggage in Shinjuku was no sweat, wide sidewalks and side streets dedicated to foot traffic makes walking around here super convenient. We didn't realize that the hotel we booked was a massive chain, like a Hilton, but with the compactness of Tokyo, there were APA hotels just across the street from each other. So it took us not a few conversations with APA receptionists to find the right one. My wife and I were told that inside Tokyo we would be fine with English, and so far that is barely true. The language barrier is very apparent, and we struggle to understand pretty frequently the english japanese people can speak. But if anything it adds to the adventure of it all.

## Shopping

![image](/images/japan-shinjuku.JPG#center)

Of course the first thing we had to do was shop! The city is filled with conbinis (cone-bee-knees, small convience stores, 7/11 brand :open_mouth:), H&M looking shopping stores, sooo many bars, girls in super tall boots holding signs I can't read so I have no idea what they are up to, and people! Its an anxious feeling, in the excited and nervous sense, feeling so small in such a huge city.

![image](/images/japan-mcdonalds.jpg#center)

We saw a 6 story McDonalds on our way to the hotel so we satisfied our cravings there then went to a store called UNIQLO. Everything is so clean here, people have been really kind, and they even laugh when I say "konichiwa" (I won't read into why, feels better to assume my humor is bilingual). After about an hour of shopping we headed back to our room. We hit the sac around 9 and sleep caught us faster than we hoped.